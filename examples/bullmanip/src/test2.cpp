//
// Created by itamar on 12/1/23.
//

#include <search/planners/wastar.hpp>
#include <search/planners/astar.hpp>
#include <search/planners/dijkstra.hpp>
#include <search/planners/arastar.hpp>
#include <search/heuristics/standard_heuristics.hpp>
#include <thread>
#include <dirent.h>

#include "common/bullet_scene_interface.hpp"
#include "action_space/bullet_action_space.hpp"
#include "common/utils.hpp"

bool EXECUTE = false;
bool READ_SETUP = true;
bool SAVE_SETUP = false;
bool GENERATE_RANDOM_STATES = true;
bool SAVE_STATES = false;
bool SAVE_STATISTICS = false;
//std::vector<double> TOP_GRASP_ORIENTATION {PI, 0, PI / 2};
std::vector<double> TOP_GRASP_ORIENTATION {0, 0, 0};
//std::vector<double> SIDE_GRASP_ORIENTATION {PI / 2, 0, PI / 2};
std::vector<double> SIDE_GRASP_ORIENTATION {PI/2, 0, 0};

// create a vector of planner params
std::vector<std::pair<std::string, std::vector<double>>> planner_params {
    {"Dijkstra", {10.0}},
    {"AStar", {10.0}},
    {"wAStar", {10.0, 40.}},
    {"ARAStar", {10.0, 100., 10.}} // time limit, init_eps, dec_eps
};

// experiments when comparing planners:
// setup 1: the 3 boxex are at the original position and we plan to grasp the first box
// setup 2: the first 2 boxes are at the original position and the third box is inside the shelf. we plan to grasp the box in the shelf
// setup 3: 1 box is inside the shelf, the second box is next to the shelf and the third box is at the original position. we plan to grasp the box in the shelf
struct SceneSetup {
    std::vector<std::vector<double>> positions;
    std::vector<std::vector<double>> half_extents;
};

void SaveSetup(SceneSetup& scene) {
    std::ofstream file;
    // define the directory
    std::string dir = "/home/itamar/bullet3/examples/bullmanip/data";
    // check if there are scenes already saved
    std::vector<std::string> files;
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == nullptr) {
        std::cout << "Error(" << errno << ") opening " << dir << std::endl;
        return;
    }
    while ((dirp = readdir(dp)) != nullptr) {
        files.emplace_back(dirp->d_name);
    }
    closedir(dp);
    // find the number of the last scene
    int last_scene = 0;
    for (auto& file_ : files) {
        if (file_.find("scene") != std::string::npos) {
            int scene_num = std::stoi(file_.substr(5, file_.find(".txt")));
            if (scene_num > last_scene) {
                last_scene = scene_num;
            }
        }
    }
    // save the scene
    std::string file_name = dir + "/scene" + std::to_string(last_scene + 1) + ".txt";
    file.open(file_name);
    // save the positions and half extents
    for (auto& pos : scene.positions) {
        file << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;
    }
    file << std::endl;
    for (auto& half_ext : scene.half_extents) {
        file << half_ext[0] << " " << half_ext[1] << " " << half_ext[2] << std::endl;
    }
    file.close();
}

void LoadSetup(SceneSetup& scene, int scene_num) {
    std::string file_name = "/home/itamar/bullet3/examples/bullmanip/data/scene" + std::to_string(scene_num) + ".txt";
    std::ifstream file(file_name);
    std::string line;
    bool positions = true;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        // while there is no empty line, read the positions. if blank line, continue and then read the half extents
        if (line.empty()) {
            positions = false;
            continue;
        }
        if (positions) {
            std::vector<double> pos(3);
            iss >> pos[0] >> pos[1] >> pos[2];
            scene.positions.push_back(pos);
        } else {
            std::vector<double> half_ext(3);
            iss >> half_ext[0] >> half_ext[1] >> half_ext[2];
            scene.half_extents.push_back(half_ext);
        }
    }
    file.close();
}

void RandomValidStates(std::vector<StateType>& states,
                       std::shared_ptr<ims::BulletActionSpace>& bas,
                       int num_states) {
    std::random_device rd;
    std::mt19937 gen(rd());
    // generate random positions in the table zone
    std::uniform_real_distribution<> dis_x(-0.4, 0.6);
    std::uniform_real_distribution<> dis_y(-0.3, 0.3);
    std::uniform_real_distribution<> dis_z(1.0, 1.2);

    // pick randomly one of the orientations
    std::vector<std::vector<double>> orientations {TOP_GRASP_ORIENTATION, SIDE_GRASP_ORIENTATION};
    std::uniform_int_distribution<> dis_or(0, 1);
    int i = 0;
    while (i < num_states) {
        StateType ws_state(7);
        ws_state[0] = dis_x(gen); ws_state[1] = dis_y(gen); ws_state[2] = dis_z(gen);
        StateType ori = orientations[dis_or(gen)];
        StateType joint_state;
        if (!bas->calculateIK(ws_state, joint_state)) {
            continue;
        }
        ims::normalizeAngles(joint_state, bas->getRobot()->joint_limits);
        ims::roundStateToDiscretization(joint_state, bas->getManipulationType()->state_discretization_);
        if (!bas->isStateValid(joint_state)) {
            continue;
        }
        // push only the first 7 joints
//        joint_state.resize(7);
        states.push_back(joint_state);
        ++i;
    }
}

void SaveStates(std::vector<StateType>& states,
                int experiment_num) {
    // define the directory
    std::string dir = "/home/itamar/bullet3/examples/bullmanip/data";
    // save the scene
    std::string file_name = dir + "/states" + std::to_string(experiment_num) + ".txt";
    std::ofstream file(file_name);
    for (auto& state : states) {
        for (auto& val : state) {
            file << val << " ";
        }
        file << std::endl;
    }
    file.close();
}

void LoadStates(std::vector<StateType>& states,
                std::shared_ptr<ims::BulletActionSpace>& bas,
                int experiment_num) {
    std::string file_name = "/home/itamar/bullet3/examples/bullmanip/data/states" + std::to_string(experiment_num) + ".txt";
    std::ifstream file(file_name);
    std::string line;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        StateType state(7);
        for (int i = 0; i < 7; ++i) {
            iss >> state[i];
        }
        bas->isStateValid(state);
        states.push_back(state);
    }
    file.close();
}


int main(int argc, char** argv) {
    // get the planner name from the command line
    std::string planner_name;
    int experiment_num = 1;
    if (argc > 1) {
        planner_name = argv[1];
        if (argc > 2) {
            experiment_num = std::stoi(argv[2]);
        }
    }  else {
        std::cout << "Planner name not provided" << std::endl;
        return -1;
    }

    std::shared_ptr<ims::JointAnglesHeuristic> heuristic = std::make_shared<ims::JointAnglesHeuristic>();

    std::vector<std::shared_ptr<ims::BestFirstSearchParams>> params_vec;
    std::vector<std::shared_ptr<ims::BestFirstSearch>> planner_vec;
    for (auto& param : planner_params) {
        if (param.first == planner_name) {
            if (param.first == "Dijkstra") {
                std::shared_ptr<ims::wAStarParams> params = std::make_shared<ims::wAStarParams>(&*heuristic, 0);
                params->time_limit_ = param.second[0];
                std::shared_ptr<ims::wAStar> planner = std::make_shared<ims::wAStar>(*params);
                params_vec.push_back(params);
                planner_vec.push_back(planner);
            }
            else if (param.first == "AStar") {
                std::shared_ptr<ims::AStarParams> params = std::make_shared<ims::AStarParams>(&*heuristic);
                params->time_limit_ = param.second[0];
                std::shared_ptr<ims::AStar> planner = std::make_shared<ims::AStar>(*params);
                params_vec.push_back(params);
                planner_vec.push_back(planner);
            }
            else if (param.first == "wAStar") {
                double weight = param.second[1];
                std::shared_ptr<ims::wAStarParams> params = std::make_shared<ims::wAStarParams>(&*heuristic,
                                                                                                weight);
                params->time_limit_ = param.second[0];
                std::shared_ptr<ims::wAStar> planner = std::make_shared<ims::wAStar>(*params);
                params_vec.push_back(params);
                planner_vec.push_back(planner);
            }
            else if (param.first == "ARAStar") {
                double init_w = param.second[1];
                double dec_eps = param.second[2];
                std::shared_ptr<ims::ARAStarParams> params = std::make_shared<ims::ARAStarParams>(&*heuristic,
                                                                                                   init_w,
                                                                                                   dec_eps);
                params->time_limit_ = param.second[0];
                params->ara_time_limit = param.second[0];
                std::shared_ptr<ims::ARAStar> planner = std::make_shared<ims::ARAStar>(*params);
                params_vec.push_back(params);
                planner_vec.push_back(planner);
            }
            else {
                std::cout << "Planner name not recognized" << std::endl;
                return -1;
            }
        }
    }

    auto planner = planner_vec[0];
    std::cout << "Planner: " << planner_name << std::endl;

    StateType discretization(7, 1);

    std::shared_ptr<ims::BulletManipulationType> bmat = std::make_shared<ims::BulletManipulationType>("/home/itamar/bullet3/examples/bullmanip/config/manip_7dof.mprim");
    ims::deg2rad(discretization);
    bmat->Discretization(discretization);

    std::shared_ptr<ims::BulletSceneInterface> bsi = std::make_shared<ims::BulletSceneInterface>(eCONNECT_GUI, false); // eCONNECT_GUI

    int table = bsi->sim_->loadURDF("/home/itamar/bullet3/examples/bullmanip/assets/table/table.urdf");

    b3RobotSimulatorLoadUrdfFileArgs args;
    args.m_startPosition.setValue(0.637, 0.51, 1.06);
    btVector3 rpy {-PI / 2, 0, PI};
    btQuaternion q = bsi->sim_->getQuaternionFromEuler(rpy);
    args.m_startOrientation.setValue(q.x(), q.y(), q.z(), q.w());
    args.m_forceOverrideFixedBase = true;
    int bookshelf = bsi->sim_->loadURDF("/home/itamar/bullet3/examples/bullmanip/assets/bookshelf/bookshelf.urdf", args);

    std::vector<std::vector<double>> positions(3);
    positions[0].resize(3); positions[1].resize(3); positions[2].resize(3);
    std::vector<double> grasp_orientation(3);
    switch (experiment_num) {
        case 1:
            positions[0] = {0.2, 0.44, 0.9};
            positions[1] = {0.45, 0.44, 0.9};
            positions[2] = {0.6, 0.44, 0.9};
//            positions[0] = {0.4, -0.25, 1.0};
//            positions[1] = {0.5, -0.25, 1.0};
//            positions[2] = {0.6, -0.25, 1.0};
            grasp_orientation = TOP_GRASP_ORIENTATION;
            break;
        case 2:
            positions[0] = {0.4, -0.25, 1.0};
            positions[1] = {0.5, -0.25, 1.0};
            // the thirs is in the shelf
            positions[2] = {0.4, 0.44, 0.9};
            grasp_orientation = SIDE_GRASP_ORIENTATION;
            break;
        case 3:
            // the first is in the shelf
            positions[0] = {0.4, 0.44, 0.9};
            positions[1] = {0.6, 0.11, 1.0};
            positions[2] = {0.25, 0.05, 1.0};
            grasp_orientation = SIDE_GRASP_ORIENTATION;
            break;
        default:
            std::cout << "Experiment number not recognized" << std::endl;
            return -1;
    }

    SceneSetup scene;
    if (READ_SETUP) {
        LoadSetup(scene, experiment_num);
    }
    std::vector<int> box_ids;
    for (int i = 0; i < 9; ++i) {
        // add boxes to scene
        b3RobotSimulatorCreateCollisionShapeArgs box_args;
        b3RobotSimulatorCreateVisualShapeArgs visual_args;
        if (!READ_SETUP){
            if (i < 3) {
                box_args.m_halfExtents.setValue(0.051/2.0, 0.185/2.0, 0.118/2.0);
                visual_args.m_halfExtents.setValue(0.051/2.0, 0.185/2.0, 0.118/2.0);
                args.m_startPosition.setValue(positions[i][0], positions[i][1], positions[i][2]);
            } else {
                // set to random position and sizes
                std::random_device rd;
                std::mt19937 gen(rd());
                std::uniform_real_distribution<> dis(0.0, 1.0);
                box_args.m_halfExtents.setValue(dis(gen) * 0.2, dis(gen) * 0.1, dis(gen) * 0.15);
                visual_args.m_halfExtents.setValue(box_args.m_halfExtents[0], box_args.m_halfExtents[1], box_args.m_halfExtents[2]);
                // on the table
                if (i < 6) {
                    std::uniform_real_distribution<> dis_x(0.4, 0.6);
                    std::uniform_real_distribution<> dis_y(-0.3, 0.0);
                    args.m_startPosition.setValue(dis_x(gen), dis_y(gen), 0.9);
                } else {
                    std::uniform_real_distribution<> dis_x(-0.6, -0.3);
                    std::uniform_real_distribution<> dis_y(-0.3, 0.40);
                    args.m_startPosition.setValue(dis_x(gen), dis_y(gen), 0.9);
                }
            }
        } else {
            box_args.m_halfExtents.setValue(scene.half_extents[i][0], scene.half_extents[i][1], scene.half_extents[i][2]);
            visual_args.m_halfExtents.setValue(scene.half_extents[i][0], scene.half_extents[i][1], scene.half_extents[i][2]);
            args.m_startPosition.setValue(scene.positions[i][0], scene.positions[i][1], scene.positions[i][2]);
        }
        box_args.m_shapeType = GEOM_BOX;
        int collision = bsi->sim_->createCollisionShape(GEOM_BOX, box_args);
        // create visual shape
        visual_args.m_shapeType = GEOM_BOX;
        int visual = bsi->sim_->createVisualShape(GEOM_BOX, visual_args);
        // create multi body
        b3RobotSimulatorCreateMultiBodyArgs mb_args;
        mb_args.m_baseMass = 2.0;
        mb_args.m_baseCollisionShapeIndex = collision;
        mb_args.m_baseVisualShapeIndex = visual;
        mb_args.m_basePosition = args.m_startPosition;
        mb_args.m_useMaximalCoordinates = false;
        mb_args.m_numLinks = 0;
        int box_id = bsi->sim_->createMultiBody(mb_args);
        // changing the color of the box
        b3RobotSimulatorChangeVisualShapeArgs change_args;
        change_args.m_objectUniqueId = box_id;
        change_args.m_hasRgbaColor = true;
        // generate a random color
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis(0.0, 1.0);
        change_args.m_rgbaColor.setValue(dis(gen), dis(gen), dis(gen), 1.0);
        bsi->sim_->changeVisualShape(change_args);
        box_ids.push_back(box_id);
        scene.positions.push_back({args.m_startPosition[0], args.m_startPosition[1], args.m_startPosition[2]});
        scene.half_extents.push_back({box_args.m_halfExtents[0], box_args.m_halfExtents[1], box_args.m_halfExtents[2]});
    }
    bsi->stepSimulation(10);
    if (SAVE_SETUP){
        SaveSetup(scene);
    }

    // add franka robot to scene
    int robot_id = bsi->addRobot("/home/itamar/bullet3/examples/pybullet/gym/pybullet_data/franka_panda/panda.urdf",
                                 std::vector<double>{0, 0, 0.76}, std::vector<double>{0, 0, 0, 1});

    if (robot_id < 0) {
        std::cout << "Failed to add robot to scene" << std::endl;
        return -1;
    }
    bsi->stepSimulation(10);

    std::shared_ptr<ims::BulletActionSpace> bas = std::make_shared<ims::BulletActionSpace>(bsi, bmat, heuristic, robot_id);
    StateType joint_state;
    bas->getCurrentJointState(joint_state);
    StateType start_state{0, -PI / 6, 0, -3 * PI / 4, 0, 3 * PI / 5, PI / 4};
//    StateType start_state(7, 0);
    bas->setToState(start_state);
    bsi->stepSimulation(1);
    bas->getCurrentJointState(joint_state);

    auto robot = bas->getRobot();
    robot->gripper_joint_ids.clear();
    robot->gripper_joint_ids.push_back(9); robot->gripper_joint_ids.push_back(10);
    robot->ee = 11;
    std::vector<std::pair<double, double>> joint_limits;
    for (int i = 0; i < 7; ++i) {
        joint_limits.emplace_back(robot->joint_limits[i].first, robot->joint_limits[i].second);
    }
    bas->openGripper();

    for (auto& id : box_ids) {
        b3RobotSimulatorChangeDynamicsArgs dynamics_args;
        dynamics_args.m_mass = 10e6;
        bsi->sim_->changeDynamics(id, -1, dynamics_args);
    }

    bsi->stepSimulation(100);

    std::vector<StateType> states;
    if (GENERATE_RANDOM_STATES) {
        RandomValidStates(states, bas, 50);
        if (SAVE_STATES) {
            SaveStates(states, experiment_num);
        }
    } else {
        LoadStates(states, bas, experiment_num);
    }

    std::vector<std::pair<int, PlannerStats>> statistics;

    std::vector<PathType> paths;
    std::vector<bool> path_found;
    for (int test {0} ;test < 50 ; ++test) {
        StateType goal_state = states[test];
        goal_state.resize(7);
        bas->setToState(start_state);
        bsi->stepSimulation(1);

        try {
            planner->initializePlanner(bas, start_state, goal_state);
        }
        catch (std::exception& e) {
            std::cout << RED << e.what() << RESET << std::endl;
        }
        // set all objects to static
        std::vector<StateType> path_;
        if (!planner->plan(path_)) {
            std::cout << RED << "[ERROR]: No path found" << RESET << std::endl;
            path_found.push_back(false);
            continue;
        }
//        bas->ShortcutPath(path_);
        path_found.push_back(true);
        paths.push_back(path_);
        PlannerStats stats = planner->reportStats();
        std::cout << "\n" << GREEN << "\t Planning time: " << stats.time << " sec" << std::endl
                         << "\t cost: " << stats.cost << std::endl
                         << "\t Path length: " << path_.size() << std::endl
                         << "\t Number of nodes expanded: " << stats.num_expanded << std::endl
                         << "\t Suboptimality: " << stats.suboptimality << RESET << std::endl;
//        start_state = goal_state;
        statistics.emplace_back(test, stats);
    }

    if(EXECUTE) {
        for (auto& id : box_ids) {
            b3RobotSimulatorChangeDynamicsArgs dynamics_args;
            dynamics_args.m_mass = 1.0;
            bsi->sim_->changeDynamics(id, -1, dynamics_args);
        }
        for (int i = 0; i < paths.size(); ++i) {
            if (!path_found[i]) {
                continue;
            }
            std::cout << "Executing path " << i << std::endl;
            bas->Move(paths[i]);
            // reset robot to start state
//            bas->setToState(start_state);
//            bsi->stepSimulation(10);
        }
    }
    if (SAVE_STATISTICS) {
       std::string file_name = "/home/itamar/bullet3/examples/bullmanip/data/statistics" + planner_name + "_" + std::to_string(experiment_num) + ".txt";
        std::ofstream file(file_name);
        double success_rate = std::accumulate(path_found.begin(), path_found.end(), 0.0) / path_found.size();
        file << "Success rate: " << success_rate << std::endl;
        file << "Planning time, Cost, Number of nodes expanded, Suboptimality" << std::endl;
        for (auto& stats_pair : statistics) {
            auto& stats = stats_pair.second;
            file << stats.time << " " << stats.cost << " " << stats.num_expanded << " " << stats.suboptimality << std::endl;
        }
        file.close();
    }

    return 0;
}


