//
// Created by itamar on 12/1/23.
//

#pragma once

// include standard libraries
#include <iostream>
#include <memory>
#include <vector>
#include <boost/filesystem.hpp>
// #include <CL/cl.h> // TODO: ADD OPTION FOR GPU OR CPU

#include "../b3RobotSimulatorClientAPI.h"
#include <bullet/LinearMath/btVector3.h>
#include <bullet/LinearMath/btAlignedObjectArray.h>
#include <bullet/btBulletCollisionCommon.h>

#include <search/common/collisions.hpp>
#include <search/common/scene_interface.hpp>
#include <search/common/types.hpp>
#include <search/common/world_objects.hpp>
#include <random>
#include <Utils/b3Clock.h>


// current path
boost::filesystem::path full_path(boost::filesystem::current_path());

namespace ims {

struct Robot {
    struct planning_group {
        std::string name;
        std::vector<std::string> links;
        std::vector<int> joint_ids{};
        std::vector<std::string> joint_names;
        std::vector<int> controllable_joints{};
        std::vector<int> ik_joint_ids{};
        std::vector<std::string> gripper_joint_names;
        std::vector<int> gripper_joint_ids{};

        std::string tool_frame {};
        int ee {};

        double motor_gains{0.05};
        double motor_forces{50.0};
    };

    int id;
    std::string urdf_string_;
    std::vector<std::string> links;
    std::vector<int> joint_ids;
    std::vector<std::string> joint_names;
    std::vector<int> controllable_joints;
    std::vector<std::pair<double, double>> joint_limits;

    std::vector<int> ik_joint_ids;
    std::vector<std::string> ik_joint_names;
    std::unordered_map<int ,std::pair<double, double>> ik_limits;
    std::vector<int> gripper_joint_ids;

    std::unordered_map<std::string, planning_group> planning_groups;
};

struct RobotPanda : Robot {

    RobotPanda() {
        planning_group arm;
        arm.name = "panda_arm";
        arm.links = {"panda_link0", "panda_link1", "panda_link2", "panda_link3",
                     "panda_link4", "panda_link5", "panda_link6", "panda_link7"};
        arm.joint_names = {"panda_joint1", "panda_joint2", "panda_joint3",
                           "panda_joint4", "panda_joint5", "panda_joint6", "panda_joint7"};
        // arm.joint_ids = {0, 1, 2, 3, 4, 5, 6};
        // arm.controllable_joints = {0, 1, 2, 3, 4, 5, 6};
        // arm.ik_joint_ids = {0, 1, 2, 3, 4, 5, 6};
        arm.gripper_joint_names = {"panda_finger_joint1", "panda_finger_joint2"};
        // arm.gripper_joint_ids = {9, 10};
        arm.tool_frame = "panda_hand";
        // arm.ee = 11;
        planning_groups["panda_arm"] = arm;
        urdf_string_ = full_path.string() + "/../../../examples/pybullet/gym/pybullet_data/franka_panda/panda.urdf";
    }

};

struct RobotPR2 : Robot {

    RobotPR2() {
        planning_group left_arm;
        left_arm.name = "left_arm";
        left_arm.links = {"l_shoulder_pan_link", "l_shoulder_lift_link", "l_upper_arm_roll_link",
                          "l_elbow_flex_link", "l_forearm_roll_link", "l_wrist_flex_link", "l_wrist_roll_link"};
        left_arm.joint_names = {"l_shoulder_pan_joint", "l_shoulder_lift_joint", "l_upper_arm_roll_joint",
                                "l_elbow_flex_joint", "l_forearm_roll_joint", "l_wrist_flex_joint", "l_wrist_roll_joint"};
        left_arm.gripper_joint_names = {"l_gripper_l_finger_joint", "l_gripper_r_finger_joint",
                                 "l_gripper_l_finger_tip_joint", "l_gripper_r_finger_tip_joint"};
        left_arm.tool_frame = "l_gripper_tool_frame";
        planning_groups["left_arm"] = left_arm;


        planning_group right_arm;
        right_arm.name = "right_arm";
        right_arm.links = {"r_shoulder_pan_link", "r_shoulder_lift_link", "r_upper_arm_roll_link",
                           "r_elbow_flex_link", "r_forearm_roll_link", "r_wrist_flex_link", "r_wrist_roll_link"};
        right_arm.joint_names = {"r_shoulder_pan_joint", "r_shoulder_lift_joint", "r_upper_arm_roll_joint",
                                 "r_elbow_flex_joint", "r_forearm_roll_joint", "r_wrist_flex_joint", "r_wrist_roll_joint"};
        right_arm.gripper_joint_names = {"r_gripper_l_finger_joint", "r_gripper_r_finger_joint",
                                  "r_gripper_l_finger_tip_joint", "r_gripper_r_finger_tip_joint"};
        right_arm.tool_frame = "r_gripper_tool_frame";
        planning_groups["right_arm"] = right_arm;

        planning_group torso;
        torso.name = "torso";
        torso.links = {"torso_lift_link"};
        torso.joint_names = {"torso_lift_joint"};
        planning_groups["torso"] = torso;
        // torso.motor_gains = 0.0;
        torso.motor_forces = 10e9;

        // urdf_string_ = full_path.string() + "/../../../examples/bullmanip/assets/pr2_description/pr2.urdf";
        urdf_string_ = full_path.string() + "/../../../examples/bullmanip/assets/PR2/pr2_no_torso_lift_tall.urdf";

    }
};


// match robot names to their class
std::unordered_map<std::string, Robot> robot_types = {
        {"panda", RobotPanda()},
        {"pr2", RobotPR2()}
};

enum ROBOTS {
    PANDA = 'panda',
    PR2 = 'pr2'
};


/// @class BulletSceneInterface
/// @brief A class that implements the SceneInterface for Bullet
/// @details This class implements the SceneInterface for Bullet. It is used to get the current state of the robot and to
/// get all information needed about the scene (e.g. obstacles, robot, etc.)
class BulletSceneInterface : public SceneInterface {
    bool verbose_ = false;
    double hz_ = 240.0;
    // collision detector
    btCollisionWorld* collision_world_;

public:
    /// @brief Constructor
    /// @param mode The mode to connect to the Bullet simulator
    explicit BulletSceneInterface(int mode = eCONNECT_SHARED_MEMORY, bool verbose = false) : verbose_(verbose) {
        sim_ = std::make_shared<b3RobotSimulatorClientAPI>();
        // get data path from BULLET_PHYSICS
        sim_->connect(mode);

        if (sim_->isConnected()) {
            std::cout << "Connected to Bullet simulator" << std::endl;
            // print all bojects in the scene
            int num_bodies = sim_->getNumBodies();
            std::cout << "Number of bodies in the scene: " << num_bodies << std::endl;
        } else {
            std::cout << "Failed to connect to Bullet simulator" << std::endl;
            std::cout << BOLDYELLOW << "WARNING: Connecting in direct mode" << RESET << std::endl;
            mode = eCONNECT_DIRECT;
            if (!sim_->connect(eCONNECT_DIRECT)) {
                throw std::runtime_error("Failed to connect to Bullet simulator");
            }
        }
        boost::filesystem::path data_path = boost::filesystem::path("~/bullet3/data");
        sim_->setAdditionalSearchPath(data_path.string());
        // check if connected to an existing server
        bool existing_server = mode == eCONNECT_EXISTING_EXAMPLE_BROWSER;
        if (existing_server) {
            sim_->syncBodies();
        } else {
            std::cout << BOLDYELLOW << "Not connected to an existing server" << RESET << std::endl;
            // remove all existing objects
            sim_->resetSimulation();
            // set gravity
            sim_->setGravity(btVector3(0, 0, -9.8));
            // set number of solver iterations
//            sim_->setNumSolverIterations(100);
            // get physics engine parameters
//            b3RobotSimulatorSetPhysicsEngineParameters args;
//            sim_->getPhysicsEngineParameters(args);
//            args.
            // load plane
//            int planeUid = sim_->loadURDF("/home/itamar/work/code/algorithms/bullmanip/assets/plane/plane.urdf");
            int planeUid = sim_->loadURDF("/home/itamar/bullet3/data/plane.urdf");
            std::cout << "planeUid = " << planeUid << std::endl;
            sim_->renderScene();
        }
        // check if GUI
        bool gui = mode == eCONNECT_GUI || mode == eCONNECT_GUI_SERVER;
        if (gui) {
            std::cout << BOLDYELLOW << "GUI mode" << RESET << std::endl;
            sim_->configureDebugVisualizer(COV_ENABLE_GUI, 0);
//            sim_->configureDebugVisualizer(COV_ENABLE_SHADOWS, 1);
//            sim_->configureDebugVisualizer(COV_ENABLE_WIREFRAME, 0);
//            sim_->configureDebugVisualizer(COV_ENABLE_TINY_RENDERER, 0);
            sim_->syncBodies();
            // check if GPU is available on the machine TODO

//            if (sim_->canSubmitCommand()) {
//                std::cout << BOLDYELLOW << "GPU mode" << RESET << std::endl;
//                sim_->configureDebugVisualizer(COV_ENABLE_RENDERING, 1);
//            } else {
//                std::cout << BOLDYELLOW << "CPU mode" << RESET << std::endl;
//                sim_->configureDebugVisualizer(COV_ENABLE_RENDERING, 0);
//            }
        }
        sim_->setTimeStep(1.0 / hz_);
        sim_->setRealTimeSimulation(true);
        sim_->setTimeOut(10);
    }

    /// @brief Destructor
    ~BulletSceneInterface() override {
        sim_->disconnect();
    }

    /// @brief Add a robot to the scene
    /// @param robot_path The path to the robot urdf
    /// @param position The position to spawn the robot at
    /// @param orientation The orientation to spawn the robot at
    /// @return The id of the robot
    int addRobot(const std::string& robot_path,
                 const btVector3 position,
                 const btQuaternion orientation) {
        b3RobotSimulatorLoadUrdfFileArgs args;
        args.m_startPosition.setValue(position.x(), position.y(), position.z());
        args.m_startOrientation.setW(orientation.w());
        args.m_startOrientation.setX(orientation.x());
        args.m_startOrientation.setY(orientation.y());
        args.m_startOrientation.setZ(orientation.z());
        // fix base
        args.m_forceOverrideFixedBase = true;
        int robot_id = sim_->loadURDF(robot_path, args);
        int numJoints = sim_->getNumJoints(robot_id);
        for (int i = 0; i < numJoints; i++)
        {
            b3JointInfo jointInfo;
            sim_->getJointInfo(robot_id, i, &jointInfo);
            if (jointInfo.m_jointName[0])
            {
                if (verbose_)
                    std::cout << "joint " << i << ": " << jointInfo.m_jointName << std::endl;
                // enable torque control
                sim_->setJointMotorControl(robot_id, i, CONTROL_MODE_VELOCITY);
            }
        }
//        stepSimulation(10);
        return robot_id;
    }

    int addRobot(const std::string& robot_path,
                 const std::vector<double>& position,
                 const std::vector<double>& orientation){
        if (position.size() != 3 || orientation.size() != 4) {
            std::cout << BOLDRED << "ERROR: position or orientation vector size is not 3 or 4" << RESET << std::endl;
            return -1;
        }
        btVector3 pos((btScalar)position[0], (btScalar)position[1], (btScalar)position[2]);
        btQuaternion ori((btScalar)orientation[0], (btScalar)orientation[1],
                         (btScalar)orientation[2], (btScalar)orientation[3]);
        return addRobot(robot_path, pos, ori);
    }

    std::shared_ptr<Robot> addRobotByType(const std::string& robot_type,
                       const std::vector<double>& position,
                       const std::vector<double>& orientation) {

        std::shared_ptr<Robot> robot = std::make_shared<Robot>(robot_types[robot_type]);
        int robot_id = addRobot(robot->urdf_string_, position, orientation);
        robot->id = robot_id;
        updateRobotData(robot);
        return robot;
    }


    /// @brief Get Joint Limits
    /// @param RobotData The RobotData to set
    /// @return A vector of pairs of joint limits
    void updateRobotData(const std::shared_ptr<Robot>& RobotData) {
        int robot_id = RobotData->id;

        for (int i{0}; i < sim_->getNumJoints(robot_id); ++i) {
            b3JointInfo joint_info;
            double lower_limit, upper_limit;
            sim_->getJointInfo(robot_id, i, &joint_info);
            RobotData->joint_names.emplace_back(joint_info.m_jointName);
            RobotData->joint_ids.emplace_back(joint_info.m_jointIndex);

            // check if it is part of one of the planning groups
            std::string planning_group_name {};
            bool gripper = false;
            for (auto& planning_group : RobotData->planning_groups) {
                if (joint_info.m_linkName == planning_group.second.tool_frame) {
                    planning_group.second.ee = i;
                }
                if (std::find(planning_group.second.joint_names.begin(),
                              planning_group.second.joint_names.end(),
                              joint_info.m_jointName) != planning_group.second.joint_names.end()) {
                    planning_group.second.joint_ids.push_back(i);
                    planning_group_name = planning_group.first;
                    break;
                } else if (std::find(planning_group.second.gripper_joint_names.begin(),
                                     planning_group.second.gripper_joint_names.end(),
                                     joint_info.m_jointName) != planning_group.second.gripper_joint_names.end()) {
                    planning_group.second.gripper_joint_ids.push_back(i);
                    planning_group_name = planning_group.first;
                    gripper = true;
                    break;
                }
            }

            if (joint_info.m_jointLowerLimit == 0 && joint_info.m_jointUpperLimit == -1) {
                lower_limit = -1e10;
                upper_limit = 1e10;
                if (joint_info.m_jointType != eFixedType) {
                    RobotData->controllable_joints.push_back(i);
                    RobotData->ik_joint_names.emplace_back(joint_info.m_jointName);
                    RobotData->ik_joint_ids.push_back(joint_info.m_jointIndex);
                    if (!planning_group_name.empty()) {
                        if (gripper) {
                            // RobotData->planning_groups[planning_group_name].gripper_joint_ids.push_back(i);
                        } else {
                            RobotData->planning_groups[planning_group_name].ik_joint_ids.push_back(i);
                            RobotData->planning_groups[planning_group_name].controllable_joints.push_back(i);
                        }
                    }
                    RobotData->ik_limits[joint_info.m_jointIndex] = std::make_pair(-2 * M_PI, 2 * M_PI);
                }
            } else if (joint_info.m_jointType != eFixedType) {
                lower_limit = joint_info.m_jointLowerLimit;
                upper_limit = joint_info.m_jointUpperLimit;
                RobotData->controllable_joints.push_back(i);
                RobotData->ik_joint_names.emplace_back(joint_info.m_jointName);
                RobotData->ik_joint_ids.push_back(joint_info.m_jointIndex);
                if (!planning_group_name.empty()) {
                    if (gripper) {
                        // RobotData->planning_groups[planning_group_name].gripper_joint_ids.push_back(i);
                    } else {
                        RobotData->planning_groups[planning_group_name].ik_joint_ids.push_back(i);
                        RobotData->planning_groups[planning_group_name].controllable_joints.push_back(i);
                    }
                }
                RobotData->ik_limits[joint_info.m_jointIndex] = std::make_pair(joint_info.m_jointLowerLimit, joint_info.m_jointUpperLimit);
            }
            RobotData->joint_limits.emplace_back(lower_limit, upper_limit);
            if (verbose_)
                std::cout << "Joint " << joint_info.m_jointName << " has limits: " << lower_limit << ", "
                << upper_limit << std::endl;
        }

        // // get the ik joint ids
        // for (int i{0}; i < RobotData->ik_joint_names.size(); ++i) {
        //     for (int j{0}; j < RobotData->joint_names.size(); ++j) {
        //         if (RobotData->ik_joint_names[i] == RobotData->joint_names[j]) {
        //             RobotData->ik_joint_ids.push_back(j);
        //             break;
        //         }
        //     }
        // }
    }

    /// @brief Forward Kinematics
    /// @param joint_state The joint state
    /// @param robot The robot
    /// @param planning_group
    /// @param link_id The id of the link
    /// @param position
    /// @param orientation
    bool forwardKinematics(const std::vector<double>& joint_state,
                           const std::shared_ptr<Robot>& robot,
                           const std::string& planning_group,
                           int link_id,
                           std::vector<double>& position,
                           std::vector<double>& orientation) {
        int counter = 0;
        for (int i : robot->planning_groups.at(planning_group).controllable_joints) {
            sim_->resetJointState(robot->id,
                                  i,
                                  joint_state[counter]);
            counter++;
        }
        b3LinkState link_state;
        sim_->getLinkState(robot->id, link_id,
                           false, true,
                           &link_state);
        for (int i{0}; i < 3; ++i) {
            position.push_back(link_state.m_worldPosition[i]);
        }
        for (int i{0}; i < 4; ++i) {
            orientation.push_back(link_state.m_worldOrientation[i]);
        }
        return true;
    }


    // @brief forward kinematics for a given planning group
    // @param joint_state The joint state
    // @param robot The robot


    ///@Addd a sphere to the scene
    ///@param radius The radius of the sphere
    ///@param position The position of the sphere
    void addSphere(double radius, const btVector3& position) {
        b3RobotSimulatorCreateVisualShapeArgs args;
        args.m_shapeType = GEOM_SPHERE;
        args.m_radius = radius;
        int sphere_id = sim_->createVisualShape(GEOM_SPHERE, args);
        b3RobotSimulatorCreateMultiBodyArgs args2;
        args2.m_baseVisualShapeIndex = sphere_id;
        args2.m_baseMass = 0.0;
        args2.m_basePosition.setValue(position.x(), position.y(), position.z());
        int sphere_body_id = sim_->createMultiBody(args2);
        // change the color of the sphere
        b3RobotSimulatorChangeVisualShapeArgs args3;
        args3.m_objectUniqueId = sphere_body_id;
        args3.m_linkIndex = -1;
        args3.m_hasRgbaColor = true;
        // yellow
        args3.m_rgbaColor.setValue(0.0, 1.0, 0.0, 1.0);
        sim_->changeVisualShape(args3);
        sim_->stepSimulation();
    }

    /// @brief check if the state is valid
    /// @param robot
    /// @param planning_group
    /// @param state The state to check
    /// @return True if the state is valid, false otherwise
    bool isStateValid(const std::shared_ptr<Robot>& robot,
        const std::string& planning_group,
        const StateType& state) {
        StateType current_state;
        // for (int i {0}; i < state.size(); ++i) {
        for (int i : robot->planning_groups[planning_group].controllable_joints) {
            b3JointSensorState joint_state;
            sim_->getJointState(robot->id, i, &joint_state);
            current_state.push_back(joint_state.m_jointPosition);
        }
        int counter {0};
        for (int i : robot->planning_groups[planning_group].controllable_joints) {
            sim_->resetJointState(robot->id, i, state[counter]);
            counter++;
        }
        bool valid = !checkCollision(robot->id);
        counter = 0;
        for (int i : robot->planning_groups[planning_group].joint_ids) {
            sim_->resetJointState(robot->id, i, current_state[counter]);
            counter++;
        }
        return valid;
    }

    /// @brief Calculate IK for a given robot and a given planning group
    /// @param robot The Robot
    /// @param planning_group The planning group to calculate IK for
    /// @param position The position to calculate IK for
    /// @param orientation The orientation to calculate IK for
    /// @param joint_state The joint state to set
    /// @return True if IK was successful, false otherwise
    bool calculateIK(std::shared_ptr<Robot>& robot,
                     const std::string& planning_group,
                     const double position[3],
                     const double orientation[4],
                     std::vector<double>& joint_state) {

        b3LinkState link_state;
        sim_->getLinkState(robot->id, robot->planning_groups[planning_group].ee,
                           false, true, &link_state);
        b3RobotSimulatorInverseKinematicArgs args;
        args.m_bodyUniqueId = robot->id;
        if (robot->planning_groups[planning_group].ee == -1) {
            std::cout << BOLDRED << "ERROR: No end effector specified" << RESET << std::endl;
            return false;
        } else {
            args.m_endEffectorLinkIndex = robot->planning_groups[planning_group].ee;
        }
        args.m_lowerLimits.resize(robot->joint_limits.size());
        args.m_upperLimits.resize(robot->joint_limits.size());
        for (int i {0} ; i < robot->joint_limits.size(); ++i) {
            args.m_lowerLimits[i] = robot->joint_limits[i].first;
            args.m_upperLimits[i] = robot->joint_limits[i].second;
        }
        // args.m_lowerLimits.resize(robot->planning_groups[planning_group].ik_joint_ids.size());
        // args.m_upperLimits.resize(robot->planning_groups[planning_group].ik_joint_ids.size());
        // int counter = 0;
        // for (const int i : robot->planning_groups[planning_group].ik_joint_ids) {
        //     args.m_lowerLimits[counter] = robot->ik_limits[i].first;
        //     args.m_upperLimits[counter] = robot->ik_limits[i].second;
        //     counter++;
        // }
        for (int i {0}; i < 3; ++i) {
            args.m_endEffectorTargetPosition[i] = position[i];
        }
        for (int i {0}; i < 4; ++i) {
            args.m_endEffectorTargetOrientation[i] = orientation[i];
            args.m_flags = B3_HAS_IK_TARGET_ORIENTATION;
        }

        // ik rest pose (set to current state if you want to seed the ik solver) TODO
        // otherwise set to random values
        std::random_device rd;
        std::mt19937 gen(rd());
        // for (const int i : robot->planning_groups[planning_group].ik_joint_ids) {
        //     std::uniform_real_distribution<> dis(robot->ik_limits[i].first,
        //         robot->ik_limits[i].second);
        //     args.m_restPoses.push_back(dis(gen));
        // }
        for (const int i : robot->ik_joint_ids) {
            std::uniform_real_distribution<> dis(robot->ik_limits[i].first,
                robot->ik_limits[i].second);
            args.m_restPoses.push_back(dis(gen));
        }
//        args.m_jointDamping.resize(joint_state.size());
//        args.m_jointDamping.fill(0.1);
        b3RobotSimulatorInverseKinematicsResults results;
        bool succ = sim_->calculateInverseKinematics(args, results);
        if (!succ)
            return false;
        // for (int j {0}; j < results.m_calculatedJointPositions.size(); ++j) {
        //     if (std::find(robot->planning_groups[planning_group].ik_joint_ids.begin(),
        //                   robot->planning_groups[planning_group].ik_joint_ids.end(), j) !=
        //         robot->planning_groups[planning_group].ik_joint_ids.end())
        //     joint_state.push_back(results.m_calculatedJointPositions[j]);
        // }
        //TODO: I am only considering the planning_group joint but it is wrong here, since the IK can be dependent on other joints.

        int counter = 0;
        for (int j : robot->ik_joint_ids) {
            if (std::find(robot->planning_groups[planning_group].ik_joint_ids.begin(),
                          robot->planning_groups[planning_group].ik_joint_ids.end(), j) !=
                robot->planning_groups[planning_group].ik_joint_ids.end())
                joint_state.push_back(results.m_calculatedJointPositions[counter]);
            counter++;
        }
        return true;
    }

    /// @brief Plot a pose in the scene
    void plotPose(int link) {
        b3RobotSimulatorAddUserDebugLineArgs args;
        args.m_lineWidth = 100.0;
        args.m_lifeTime = 1000.0;
        args.m_parentLinkIndex = link;
        btVector3 from = btVector3(0, 0, 0);
        btVector3 to = btVector3(1, 0, 0);
        sim_->addUserDebugLine(from, to, args);
        sim_->stepSimulation();
    }

    /// @brief set sim frequency
    /// @param hz The frequency to set
    void setHz(double hz) {
        hz_ = hz;
        sim_->setTimeStep(1.0 / hz_);
    }

    /// @brief step the simulation
    /// @param steps The number of steps to performs
    void stepSimulation(int steps = 1) {
        for (int i{0}; i < steps; ++i) {
            sim_->stepSimulation();
            // slow down the simulation
            b3Clock::usleep(10e6 * 1. / hz_);
        }
    }

    /// @brief perform collision checking
    /// @param robot_id The id of the robot to check
    bool checkCollision(int robot_id = -1) {
        b3RobotSimulatorGetContactPointsArgs args;
        if (robot_id != -1)
            args.m_bodyUniqueIdA = robot_id;
        b3ContactInformation info;
        sim_->getContactPoints(args, &info);
        if (verbose_ && info.m_numContactPoints > 0)
            std::cout << "Number of contact points: " << info.m_numContactPoints << std::endl;
        return info.m_numContactPoints > 0;
    }

    bool Move(std::shared_ptr<Robot>& robot,
        const std::string& planning_group,
        const std::vector<std::vector<double>>& joint_path) {

        auto* args = new b3RobotSimulatorJointMotorArrayArgs(CONTROL_MODE_POSITION_VELOCITY_PD,
                                                             static_cast<int>(joint_path[0].size()));
        int* joint_ids = new int[joint_path[0].size()];

        auto* forces = new double[joint_path[0].size()];
        auto* gains = new double[joint_path[0].size()];
        for (int i{0}; i < joint_path[0].size(); ++i) {
            joint_ids[i] = robot->planning_groups[planning_group].joint_ids[i];
            // forces[i] = robot->planning_groups[planning_group].motor_forces;
            // gains[i] = robot->planning_groups[planning_group].motor_gains;
        }
        args->m_jointIndices = joint_ids;
        // args->m_forces = forces;
        // args->m_kps = gains;

        for (int i{0}; i < joint_path.size(); ++i) {
            auto* target_positions = new double[joint_path.at(i).size()+1];
            for (int j{0}; j < joint_path.at(i).size(); ++j) {
                target_positions[j] = joint_path.at(i).at(j);
            }
            args->m_targetPositions = target_positions;
            sim_->setJointMotorControlArray(robot->id, *args);
            stepSimulation(5);
            // std::cout << "Step: " << i << std::endl;
            delete[] target_positions;
        }
        // stepSimulation(50);
        std::cout << "Done" << std::endl;
        delete args;
        std::cout << "Deleted args" << std::endl;
        delete[] joint_ids;
        std::cout << "Deleted joint_ids" << std::endl;
        delete[] forces;
        std::cout << "Deleted forces" << std::endl;
        delete[] gains;
        std::cout << "Deleted gains" << std::endl;
        return true;
    }

    bool Move(std::shared_ptr<Robot>& robot,
        const std::string& planning_group,
        const std::vector<std::vector<double>>& joint_path, const std::vector<int>& joint_ids) {
        auto* args = new b3RobotSimulatorJointMotorArrayArgs(CONTROL_MODE_POSITION_VELOCITY_PD,
                                                             static_cast<int>(joint_path[0].size()));
        int* joint_ids_ = new int[joint_path[0].size()];

        auto* forces = new double[joint_path[0].size()];
        auto* gains = new double[joint_path[0].size()];
        for (int i{0}; i < joint_path[0].size(); ++i) {
            joint_ids_[i] = joint_ids[i];
            forces[i] = robot->planning_groups[planning_group].motor_forces;
            gains[i] = robot->planning_groups[planning_group].motor_gains;
        }
        args->m_jointIndices = joint_ids_;
        args->m_forces = forces;
        args->m_kps = gains;

        for (int i{0}; i < joint_path.size(); ++i) {
            auto* target_positions = new double[joint_path[i].size()];
            for (int j{0}; j < joint_path[i].size(); ++j) {
                target_positions[j] = joint_path[i][j];
            }
            args->m_targetPositions = target_positions;
            sim_->setJointMotorControlArray(robot->id, *args);
            stepSimulation(1);
            delete[] target_positions;
        }
        stepSimulation(50);
        delete args;
        delete[] joint_ids_;
        delete[] forces;
        delete[] gains;
        return true;
    }

    bool setJointState(int robot_id, const std::vector<double>& joint_state) {
        for (int i{0}; i < joint_state.size(); ++i) {
            sim_->resetJointState(robot_id, i, joint_state[i]);
        }
        stepSimulation();
        return true;
    }

    bool setJointState(int robot_id,
        const StateType& joint_state,
        const std::vector<int>& joint_ids) {
        int counter {0};
        for (auto i : joint_ids) {
            sim_->resetJointState(robot_id, i, joint_state[counter]);
            counter++;
        }
        stepSimulation();
        return true;
    }

    bool getCurrentJointState(std::shared_ptr<Robot>& robot,
        const std::string& planning_group,
        std::vector<double>& joint_state) {
        for (int i{0}; i < robot->planning_groups[planning_group].joint_ids.size(); ++i) {
            b3JointSensorState joint_sensor_state;
            sim_->getJointState(robot->id, robot->joint_ids[i], &joint_sensor_state);
            joint_state.push_back(joint_sensor_state.m_jointPosition);
        }
        return true;
    }

    void ShortcutPath(std::shared_ptr<Robot>& robot,
        const std::string& planning_group,
        PathType& path, int num_iterations = 100) {
        for (int i{0}; i < num_iterations; ++i) {
            int start = rand() % path.size();
            int end = rand() % path.size();
            if (start == end)
                continue;
            if (start > end)
                std::swap(start, end);
            if (end - start < 2)
                continue;
            StateType start_state = path[start];
            StateType end_state = path[end];
            StateType new_state;
            for (int j{0}; j < start_state.size(); ++j) {
                new_state.push_back(start_state[j]);
            }
            for (int j{0}; j < end_state.size(); ++j) {
                new_state.push_back(end_state[j]);
            }
            if (isStateValid(robot, planning_group, new_state)) {
                path.erase(path.begin() + start + 1, path.begin() + end);
                path.insert(path.begin() + start + 1, new_state);
            }
        }
    }

    void densifyPath(const PathType& path, PathType& new_path, double step_size = 0.1) {
        for (int i{0}; i < path.size() - 1; ++i) {
            StateType start_state = path[i];
            StateType end_state = path[i + 1];
            double distance = 0;
            for (int j{0}; j < start_state.size(); ++j) {
                distance += pow(start_state[j] - end_state[j], 2);
            }
            distance = sqrt(distance);
            int num_steps = distance / step_size;
            if (num_steps == 0)
                num_steps = 1;
            for (int j{0}; j < num_steps; ++j) {
                StateType new_state;
                for (int k{0}; k < start_state.size(); ++k) {
                    new_state.push_back(start_state[k] + (end_state[k] - start_state[k]) * j / num_steps);
                }
                new_path.push_back(new_state);
            }
        }
    }

    std::shared_ptr<b3RobotSimulatorClientAPI> sim_;
};
}
