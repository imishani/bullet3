//
// Created by itamar on 12/1/23.
//

#pragma once

#include <vector>
#include <eigen3/Eigen/Geometry>

#include <search/common/types.hpp>

namespace ims {

    /// \brief A function that converts a state from radians to degrees
    /// \param state The state
    template <typename T>
    void rad2deg(std::vector<T>& state, const std::vector<bool> & valid_mask = std::vector<bool>()){
        bool is_valid_mask_passed = !valid_mask.empty();

        for (size_t i = 0; i < state.size(); ++i) {
            // If the valid mask is passed, then check if the dimension is valid.
            if (is_valid_mask_passed){
                if (!valid_mask[i]){
                    continue;
                }
            }

            // Convert the dimension, if it is valid according to the mask.
            state[i] = state[i] * 180 / M_PI;
        }
    }

    /// \brief A function that converts a state from degrees to radians
    /// \param state The state
    /// \param valid_mask The valid mask of the state. Dimensions that are true, get converted. Others are not.
    template <typename T>
    void deg2rad(std::vector<T>& state, const std::vector<bool> & valid_mask = std::vector<bool>()){

        // If the mask is not passed, then all the dimensions are assumed to be valid.
        bool is_valid_mask_passed = !valid_mask.empty();

        for (size_t i = 0; i < state.size(); ++i) {
            // If the valid mask is passed, then check if the dimension is valid.
            if (is_valid_mask_passed){
                if (!valid_mask[i]){
                    continue;
                }
            }

            // Convert the dimension, if it is valid according to the mask.
            state[i] = state[i] * M_PI / 180;
        }
    }

    inline void negateState(StateType& state, const std::vector<bool> & valid_mask = std::vector<bool>()){
        // If the mask is not passed, then all the dimensions are assumed to be valid.
        bool is_valid_mask_passed = !valid_mask.empty();

        for (size_t i = 0; i < state.size(); ++i) {
            // If the valid mask is passed, then check if the dimension is valid.
            if (is_valid_mask_passed){
                if (!valid_mask[i]){
                    continue;
                }
            }

            // Convert the dimension, if it is valid according to the mask.
            state[i] = -state[i];
        }
    }

    /// \brief A function that dealing with the discontinuity of the joint angles
    /// \param state The state to check
    /// \param joint_limits The joint limits
    /// \param valid_mask The valid mask of the state. Dimensions that are true, get normalized. Others are not.
    /// \return The state with the joint angles in the range of the joint limits
    template <typename T>
    inline void normalizeAngles(std::vector<T>& state, std::vector<std::pair<T, T>>& joint_limits, const std::vector<bool> & valid_mask = std::vector<bool>()){
        // If the mask is not passed, then all the dimensions are assumed to be valid.
        bool is_valid_mask_passed = !valid_mask.empty();

        // Iterate through the state and normalize the dimensions that are valid.
        for (int i = 0; i < state.size(); ++i) {
            // If the valid mask is passed, then check if the dimension is valid.
            if (is_valid_mask_passed){
                if (!valid_mask[i]){
                    continue;
                }
            }

            // Normalize the dimension, if it is not in the range of the joint limits and it is valid according to the mask.
            if (state[i] > joint_limits[i].second){
                state[i] = state[i] - 2*M_PI;
            }
            else if (state[i] < joint_limits[i].first){
                state[i] = state[i] + 2*M_PI;
            }
        }
    }

    /// \brief A function that dealing with the discontinuity of the joint angles
    /// \note This function assumes that the joint limits are [-pi, pi]
    /// \param state The state to check
    /// \param valid_mask The valid mask of the state. Dimensions that are true, get normalized. Others are not.
    /// \return The state with the joint angles in the range of [-pi, pi]
    template <typename T>
    inline void normalizeAngles(std::vector<T>& state, const std::vector<bool> & valid_mask = std::vector<bool>()){
        bool is_valid_mask_passed = !valid_mask.empty();
        // Iterate through the state and normalize the dimensions that are valid.
        for (int i = 0; i < state.size(); ++i) {

            // If the valid mask is passed, then check if the dimension is valid.
            if (is_valid_mask_passed){
                if (!valid_mask[i]){
                    continue;
                }
            }

            // Normalize the dimension, if it is not in the range of the joint limits and it is valid according to the mask.
            if (state[i] > M_PI){
                state[i] = state[i] - 2*M_PI;
            }
            else if (state[i] < -M_PI){
                state[i] = state[i] + 2*M_PI;
            }
        }
    }


    template <typename T>
    void checkFixGimbalLock(T y, T p, T r){
        // check if current state in gimbal lock
        /// TODO: Implement

    }

    template <typename T>
    void get_euler_zyx(const Eigen::Matrix<T, 3, 3>& rot, T& y, T& p, T& r)
    {
        y = std::atan2(rot(1, 0), rot(0, 0));
        p  = std::atan2(-rot(2, 0), std::sqrt(rot(2, 1) * rot(2, 1) + rot(2, 2) * rot(2, 2)));
        r = std::atan2(rot(2, 1), rot(2, 2));
    }

    template <typename T>
    void get_euler_zyx(const Eigen::Quaternion<T>& rot, T& y, T& p, T& r)
    {
        Eigen::Matrix<T, 3, 3> R(rot);
        get_euler_zyx(R, y, p, r);
    }

    template <typename T>
    void from_euler_zyx(T y, T p, T r, Eigen::Matrix<T, 3, 3>& rot)
    {
        rot = Eigen::AngleAxis<T>(y, Eigen::Matrix<T, 3, 1>::UnitZ()) *
            Eigen::AngleAxis<T>(p, Eigen::Matrix<T, 3, 1>::UnitY()) *
            Eigen::AngleAxis<T>(r, Eigen::Matrix<T, 3, 1>::UnitX());
    }

    template <typename T>
    void from_euler_zyx(T y, T p, T r, Eigen::Quaternion<T>& q)
    {
        Eigen::Matrix<T, 3, 3> R;
        from_euler_zyx(y, p, r, R);
        q = Eigen::Quaternion<T>(R);
    }

    template <typename T>
    void normalize_euler_zyx(T& y, T& p, T& r)
    {
        Eigen::Matrix<T, 3, 3> rot;
        from_euler_zyx(y, p, r, rot);
        get_euler_zyx(rot, y, p, r);
    }

    template <typename T>
    void normalize_euler_zyx(T* angles) // in order r, p, y
    {
        Eigen::Matrix<T, 3, 3> rot;
        from_euler_zyx(angles[2], angles[1], angles[0], rot);
        get_euler_zyx(rot, angles[2], angles[1], angles[0]);
    }

    /// \brief A function that takes the discretization vector and a state, and round it to the nearest discretization value
    /// \param state The state to check
    /// \param discretization The discretization vector
    inline void roundStateToDiscretization(StateType& state, const StateType& discretization){
        for (int i = 0; i < state.size(); ++i) {
            state[i] = round(state[i]/discretization[i])*discretization[i];
        }
    }

}


