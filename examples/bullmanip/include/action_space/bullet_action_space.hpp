//
// Created by itamar on 12/1/23.
//

#pragma once

#include "../common/bullet_scene_interface.hpp"
#include "../common/utils.hpp"

#include <bullet/LinearMath/btQuaternion.h>

#include <search/action_space/action_space.hpp>
#include <utility>

namespace ims {

struct BulletManipulationType : ActionType
{

    /// @brief Constructor
    explicit BulletManipulationType() : action_type_(ActionType::MOVE),
                                        space_type_(SpaceType::ConfigurationSpace),
                                        prim_file_name_("../../config/manip_7dof.mprim"),
                                        max_action_(0.0){
    };

    /// @brief Constructor with motion primitives file given
    explicit BulletManipulationType(std::string mprimFile) : action_type_(ActionType::MOVE),
                                                             space_type_(SpaceType::ConfigurationSpace),
                                                             prim_file_name_(std::move(mprimFile)),
                                                             max_action_(0.0){
    };


    /// @brief Constructor with adaptive motion primitives given


    /// @brief Destructor
    ~BulletManipulationType() override = default;

    /// @brief The type of the action
    enum class ActionType
    {
        MOVE,
        GRASP,
        RELEASE
    };

    enum class SpaceType
    {
        ConfigurationSpace,
        WorkSpace
    };
    /// @{ getters and setters
    /// @brief Get the action type
    /// @return The action type
    ActionType getActionType() const
    {
        return action_type_;
    }

    /// @brief Set the action type
    /// @param ActionType The action type
    void setActionType(ActionType ActionType)
    {
        action_type_ = ActionType;
    }

    /// @brief Get the space type
    /// @return The space type
    SpaceType getSpaceType() const
    {
        return space_type_;
    }

    /// @brief Set the space type
    /// @param SpaceType The space type
    void setSpaceType(SpaceType SpaceType)
    {
        space_type_ = SpaceType;
    }
    /// @}

    void Discretization(StateType &state_des) override
    {
        state_discretization_ = state_des;
    }

    void readMPfile()
    {
        std::ifstream file(prim_file_name_);
        std::string line;
        std::vector<std::vector<double>> mprim;
        switch (space_type_) {
            case SpaceType::ConfigurationSpace: {
                int tot_prim {0}, dof{0}, num_short_prim{0};
                int i{0};
                while (std::getline(file, line))
                {
                    if (i == 0)
                    {
                        // First line being with: "Motion_Primitives(degrees): " and then three numbers. Make sure the line begins with the string and then get the numbers
                        std::string first_line = "Motion_Primitives(degrees): ";
                        // Check if the line begins with the string
                        if (line.find(first_line) != 0)
                        {
                            std::cout << RED << "ERROR: The first line of the motion primitives file should begin with: "
                                                "" << RESET << first_line << std::endl;
                        }
                        // Get the numbers
                        std::istringstream iss(line.substr(first_line.size()));
                        if (!(iss >> tot_prim >> dof >> num_short_prim))
                        {
                            std::cout << RED << "ERROR: The first line of the motion primitives file should begin with: "
                                                "" << RESET << first_line << std::endl;
                        }
                        i++;
                        continue;
                    }
                    std::istringstream iss(line);
                    std::vector<double> line_;
                    double num;
                    while (iss >> num)
                    {
                        line_.push_back(num);
                        if (abs(num * M_PI / 180.0) > max_action_)
                        {
                            max_action_ = abs(num * M_PI / 180.0);
                        }
                    }
                    // Check if short or long primitive (the last num_short_prim lines are short)
                    if (i > tot_prim - num_short_prim)
                    {
                        short_mprim_.push_back(line_);
                    }
                    else
                    {
                        long_mprim_.push_back(line_);
                    }
                    i++;
                }
            }
                break;
            case SpaceType::WorkSpace: {
                int tot_ptim {0}, positions_prims {0}, orientations_prims {0};
                int i{0};
                while (std::getline(file, line))
                {
                    if (i == 0)
                    {
                        // First line being with: "Motion_Primitives(degrees): " and then three numbers. Make sure the line begins with the string and then get the numbers
                        std::string first_line = "Motion_Primitives(meters/degrees): ";
                        // Check if the line begins with the string
                        if (line.find(first_line) != 0)
                        {
                            std::cout << RED << "ERROR: The first line of the motion primitives file should begin with: "
                                                "" << RESET << first_line << std::endl;
                        }
                        // Get the numbers
                        std::istringstream iss(line.substr(first_line.size()));
                        if (!(iss >> tot_ptim >> positions_prims >> orientations_prims))
                        {
                            std::cout << RED << "ERROR: The first line of the motion primitives file should begin with: "
                                                "" << RESET << first_line << std::endl;
                        }
                        i++;
                        continue;
                    }
                    std::istringstream iss(line);
                    std::vector<double> line_;
                    double num;
                    while (iss >> num)
                    {
                        line_.push_back(num);
                        if (abs(num) > max_action_)
                        {
                            max_action_ = abs(num);
                        }
                    }
                    // TODO: Currently I am using short_mprim_ to store the work space motion primitives. This is not correct.
                    short_mprim_.push_back(line_);
                    i++;
                }
            }

        }
    }

    /// @brief Get the possible actions
    /// @return A vector of all possible actions
    std::vector<Action> getPrimActions() override
    {
        if (short_mprim_.empty() && long_mprim_.empty())
        {
            readMPfile();
        }
        if (actions_.empty()) {
            switch (action_type_) {
                case ActionType::MOVE:
                    switch (space_type_) {
                        case SpaceType::ConfigurationSpace: {
                            // TODO: Add snap option
                            std::vector<std::vector<double>> mprim;
                            mprim.insert(mprim.end(), long_mprim_.begin(), long_mprim_.end());
                            mprim.insert(mprim.end(), short_mprim_.begin(), short_mprim_.end());
                            for (auto &action_ : mprim) {
                                // convert from degrees to radians
                                for (auto &num : action_) {
                                    num = num * M_PI / 180.0;
                                }
                                actions_.push_back(action_);
                                // get the opposite action
                                for (auto &num : action_) {
                                    num = -num;
                                }
                                actions_.push_back(action_);
                            }
                        }
                            break;
                        case SpaceType::WorkSpace: {
                            std::vector<std::vector<double>> mprim;
                            mprim.insert(mprim.end(), short_mprim_.begin(), short_mprim_.end());
                            for (auto &action_ : mprim) {
                                // make an inverted action
                                Action inverted_action(action_.size());
                                inverted_action[0] = -action_[0];
                                inverted_action[1] = -action_[1];
                                inverted_action[2] = -action_[2];
                                inverted_action[3] = -action_[3];
                                inverted_action[4] = -action_[4];
                                inverted_action[5] = -action_[5];
                                // convert from euler angles to quaternions
                                btQuaternion q;
                                // get the quaternion from rpy

                                q.setEulerZYX(static_cast<btScalar>(action_[5] * M_PI / 180.0),
                                            static_cast<btScalar>(action_[4] * M_PI / 180.0),
                                            static_cast<btScalar>(action_[3] * M_PI / 180.0));

                                // check from antipodal quaternions
                                btScalar sign = 1;
                                if (q.w() < 0) {
                                    sign = -1;
                                }
                                action_.resize(7);
                                action_[3] = sign * q.x();
                                action_[4] = sign * q.y();
                                action_[5] = sign * q.z();
                                action_[6] = sign * q.w();
                                actions_.push_back(action_);
                                // get the opposite action
                                q.setEulerZYX(static_cast<btScalar>(inverted_action[5] * M_PI / 180.0),
                                              static_cast<btScalar>(inverted_action[4] * M_PI / 180.0),
                                              static_cast<btScalar>(inverted_action[3] * M_PI / 180.0));
                                // check from antipodal quaternions
                                sign = 1;
                                if (q.w() < 0) {
                                    sign = -1;
                                }
                                inverted_action.resize(7);
                                inverted_action[3] = sign * q.x();
                                inverted_action[4] = sign * q.y();
                                inverted_action[5] = sign * q.z();
                                inverted_action[6] = sign * q.w();
                                actions_.push_back(inverted_action);
                            }
                        }
                            break;
                    }
                    break;
                case ActionType::GRASP:break;
                case ActionType::RELEASE:break;
            }
            return actions_;
        } else {
            return actions_;
        }
    }

    /// @brief Get adaptive motion primitives
    /// @param start_dist The distance from the start
    /// @param goal_dist The distance from the goal
    /// @return A vector of actions
    std::vector<Action> getAdaptiveActions(double &start_dist, double &goal_dist) {
        if (short_mprim_.empty() && long_mprim_.empty())
        {
            readMPfile();
        }
        actions_.clear();

        bool use_short = false;
        if (mprim_active_type_.short_dist.first && (goal_dist < mprim_active_type_.short_dist.second))
            use_short = true;

        //  Add the short or long primitives.
        for (auto &action_prim : use_short ? short_mprim_ : long_mprim_) {
            std::vector<double> action, action_rev;

            // Convert to radians and add a positive and negative version.
            action = action_prim;
            deg2rad(action);
            // Add the forward action to the list of actions.
            actions_.push_back(action);
            for (auto &num : action) {
                action_rev.push_back(-num);
            }
            // If the reverse action is different from the forward action, add it to the list of actions.
            if (action != action_rev) {
                actions_.push_back(action_rev);
            }
        }

        if (mprim_active_type_.snap_xyz.first && goal_dist < mprim_active_type_.snap_xyz.second)
            actions_.push_back({INF_DOUBLE, INF_DOUBLE, INF_DOUBLE,
                                INF_DOUBLE, INF_DOUBLE, INF_DOUBLE});  // TODO: Fix this to make it better designed
        if (mprim_active_type_.snap_rpy.first && goal_dist < mprim_active_type_.snap_rpy.second)
            actions_.push_back({INF_DOUBLE, INF_DOUBLE, INF_DOUBLE,
                                INF_DOUBLE, INF_DOUBLE, INF_DOUBLE});  // TODO: Fix this to make it better designed
        if (mprim_active_type_.snap_xyzrpy.first && goal_dist < mprim_active_type_.snap_xyzrpy.second) {
            actions_.push_back({INF_DOUBLE, INF_DOUBLE, INF_DOUBLE,
                                INF_DOUBLE, INF_DOUBLE, INF_DOUBLE});  // TODO: Fix this to make it better designed
        }
        return actions_;
    }


    /// @brief Set values in the motion primitive active type.
    /// @param short_dist The short distance threshold
    /// @param long_dist The long distance threshold
    /// @param snap_xyz The snap xyz threshold
    /// @param snap_rpy The snap rpy threshold
    /// @param snap_xyzrpy The snap xyzrpy threshold
    void setMprimActiveType(std::pair<bool, double> short_dist,
                            std::pair<bool, double> long_dist,
                            std::pair<bool, double> snap_xyz,
                            std::pair<bool, double> snap_rpy,
                            std::pair<bool, double> snap_xyzrpy) {
        mprim_active_type_.short_dist = short_dist;
        mprim_active_type_.long_dist = long_dist;
        mprim_active_type_.snap_xyz = snap_xyz;
        mprim_active_type_.snap_rpy = snap_rpy;
        mprim_active_type_.snap_xyzrpy = snap_xyzrpy;
    }

    /// @brief Motion primitive active type: Used for adaptive motion primitives, given a few motion primitives,
    /// which one is active at a given time and it's threshold
    struct MotionPrimitiveActiveType
    {
        std::pair<bool, double> short_dist = std::make_pair(true, 6.0);
        std::pair<bool, double> long_dist = std::make_pair(true, 0.4);
        std::pair<bool, double> snap_xyz = std::make_pair(false, 0.2);
        std::pair<bool, double> snap_rpy = std::make_pair(false, 0.2);
        std::pair<bool, double> snap_xyzrpy = std::make_pair(true, 10.0);
    };

    ActionType action_type_;
    SpaceType space_type_;
    std::string prim_file_name_;
    MotionPrimitiveActiveType mprim_active_type_;

    std::vector<Action> actions_;
    std::vector<Action> short_mprim_;
    std::vector<Action> long_mprim_;

    std::vector<bool> mprim_enabled_;
    std::vector<double> mprim_thresh_;
    double max_action_;
};

/// @brief A class that implements the action space for the bullet simulator
/// @class BulletActionSpace
class BulletActionSpace : public ActionSpace
{
public:
    std::shared_ptr<BulletManipulationType> manipulation_type_;
    std::shared_ptr<BulletSceneInterface> scene_interface_;
    std::shared_ptr<JointAnglesHeuristic> heuristic_;
    std::shared_ptr<Robot> robot_;
    std::string planning_group_;

public:
    /// @brief Constructor
    /// @param scene_interface A pointer to the scene interface
    /// @param manipulation_type A pointer to the manipulation type
    /// @param heuristic
    /// @param robot
    /// @param planning_group
    BulletActionSpace(const std::shared_ptr<BulletSceneInterface>& scene_interface,
                      const std::shared_ptr<BulletManipulationType>& manipulation_type,
                      const std::shared_ptr<JointAnglesHeuristic>& heuristic,
                      const std::shared_ptr<Robot>& robot,
                      std::string planning_group) :
                      manipulation_type_(manipulation_type),
                      scene_interface_(scene_interface),
                      heuristic_(heuristic),
                      planning_group_(std::move(planning_group)){
        robot_ = robot;
        // scene_interface_->updateRobotData(robot_);
    }


    /// @brief Destructor
    ~BulletActionSpace() override = default;

    std::shared_ptr<Robot> getRobot() {
        return robot_;
    }

    std::shared_ptr<BulletManipulationType> getManipulationType() {
        return manipulation_type_;
    }

    /// @brief Set the gripper state
    /// @param state The gripper state
    bool setGripperState(const std::vector<double>& state) {
        if (robot_->planning_groups[planning_group_].gripper_joint_ids.empty() ||
            state.size() != robot_->planning_groups[planning_group_].gripper_joint_ids.size()) {
            std::cout << RED << "ERROR: Gripper joint ids are empty" << RESET << std::endl;
            return false;
        } else {
            return scene_interface_->setJointState(robot_->id, state,
                robot_->planning_groups[planning_group_].gripper_joint_ids);
        }
    }

    bool openGripper() {
        std::vector<double> state(robot_->planning_groups[planning_group_].gripper_joint_ids.size());
        for (int i{0}; i < state.size(); i++) {
//            state[i] = robot_.joint_limits[robot_.gripper_joint_ids[i]].first;
            state[i] = 1.0;
        }
        return Control(state, robot_->planning_groups[planning_group_].gripper_joint_ids);
    }

    bool closeGripper() {
        std::vector<double> state(robot_->planning_groups[planning_group_].gripper_joint_ids.size());
        for (int i{0}; i < state.size(); i++) {
            state[i] = robot_->joint_limits[robot_->planning_groups[planning_group_].gripper_joint_ids[i]].second;
        }
        return Control(state, robot_->planning_groups[planning_group_].gripper_joint_ids);
    }


    /// @brief Get Actions
    /// @param state_id
    /// @param actions_seq
    /// @param check_validity
    void getActions(int state_id,
                    std::vector<ActionSequence> &actions_seq,
                    bool check_validity) override {
        RobotState* curr_state = getRobotState(state_id);
        StateType curr_state_val = curr_state->state;
        // visualize the current state
        StateType ee_position;
        StateType ee_orientation;
        scene_interface_->forwardKinematics(curr_state_val, robot_, planning_group_,
                                            robot_->planning_groups[planning_group_].ee,
                                            ee_position, ee_orientation);
        btVector3 vec_position(ee_position[0], ee_position[1], ee_position[2]);
        scene_interface_->addSphere(0.02, vec_position);

        double goal_dist;
        heuristic_->getHeuristic(curr_state_val, heuristic_->goal_, goal_dist);
        double start_dist;
        heuristic_->getHeuristic(curr_state_val, heuristic_->start_, start_dist);
        auto actions = manipulation_type_->getAdaptiveActions(start_dist, goal_dist);
        for (int i {0} ; i < actions.size() ; i++) {
            auto action = actions[i];
            ActionSequence action_seq{curr_state_val};
            if (action[0] == INF_DOUBLE) {
                // snap action
                action_seq.push_back(heuristic_->goal_);
            } else {
                // push back the new state after the action
                StateType next_state_val(curr_state_val.size());
                std::transform(curr_state_val.begin(),
                               curr_state_val.end(),
                               action.begin(),
                               next_state_val.begin(),
                               std::plus<>());
                action_seq.push_back(next_state_val);
            }
            actions_seq.push_back(action_seq);
        }
    }


    /// @brief Set the manipulation space type
    /// @param SpaceType The manipulation type
    void setManipActionType(BulletManipulationType::SpaceType SpaceType)
    {
        manipulation_type_->setSpaceType(SpaceType);
    }

    BulletManipulationType::SpaceType getManipActionType()
    {
        return manipulation_type_->getSpaceType();
    }

    bool isStateValid(const StateType &state_val) override {
        return scene_interface_->isStateValid(robot_, planning_group_, state_val);
    }

    static PathType interpolatePath(const StateType &start,
                                    const StateType &end,
                                    const double resolution = 0.05)
    {
        // TODO: Currently only works for configuration space
        assert(start.size() == end.size());
        PathType path;
        // get the maximum distance between the two states
        double max_distance{0.0};
        for (int i{0}; i < start.size(); i++)
        {
            double distance = std::abs(start[i] - end[i]);
            if (distance > max_distance)
            {
                max_distance = distance;
            }
        }
        // calculate the number of steps
        int steps = std::ceil(max_distance / resolution);
        // interpolate the path
        for (int i{0}; i < steps; i++)
        {
            StateType state;
            for (int j{0}; j < start.size(); j++)
            {
                state.push_back(start[j] + (end[j] - start[j]) * i / steps);
            }
            path.push_back(state);
        }
        return path;
    }

    bool isPathValid(const PathType &path) override {
        for (int i{0}; i < path.size() - 1; i++)
        {
            if (!isStateToStateValid(path[i], path[i + 1]))
            {
                return false;
            }
        }
        return true;
    }

    bool isStateToStateValid(const StateType &start,
                             const StateType &end) {
        PathType path = interpolatePath(start, end);
        for (auto &state : path)
            if (!isStateValid(state)) {
                return false;
            }
        return true;
    }

    virtual bool getSuccessorsWs(int curr_state_ind,
                                 std::vector<int>& successors,
                                 std::vector<double> &costs)
    {   // TODO: Currently only works for configuration space
        return false;
    }

    virtual bool getSuccessorsCs(int curr_state_ind,
                                 std::vector<int>& successors,
                                 std::vector<double> &costs)
    {
        std::vector<ActionSequence> actions;
        getActions(curr_state_ind, actions, false);

        // get the successors
        for (auto &action : actions)
        {
            // the first state is the current state and the last state is the successor
            auto curr_state_val = action.front();
            auto new_state_val = action.back();
            // normalize the angles
            // joint_limits based on planning group
            std::vector<std::pair<double, double>> joint_limits;
            for (auto &joint_id : robot_->planning_groups[planning_group_].joint_ids)
            {
                joint_limits.push_back(robot_->joint_limits[joint_id]);
            }
            normalizeAngles(new_state_val, joint_limits);
            // discretize the state
            roundStateToDiscretization(new_state_val, manipulation_type_->state_discretization_);
            // check if the state went through discontinuity
            bool discontinuity{false};
            // check for maximum absolute action
            for (int i{0}; i < curr_state_val.size(); i++)
            {
                if (new_state_val[i] < robot_->joint_limits[robot_->planning_groups[planning_group_].joint_ids.at(i)].first ||
                    new_state_val[i] > robot_->joint_limits[robot_->planning_groups[planning_group_].joint_ids.at(i)].second)
                {
                    discontinuity = true;
                    break;
                }
            }

            if (!discontinuity && isStateToStateValid(curr_state_val, new_state_val))
            {

                // create a new state
                int next_state_ind = getOrCreateRobotState(new_state_val);
                // add the state to the successors
                successors.push_back(next_state_ind);
                // add the cost
                double distance{0.0};
                for (int i{0}; i < curr_state_val.size(); i++)
                {
                    distance += std::pow(curr_state_val[i] - new_state_val[i], 2);
                }
//                costs.push_back(std::sqrt(distance));
//                costs.push_back(1.0*M_PI/180.0);
                costs.push_back(1.0);
            }
        }
        return true;
    }

    bool getSuccessors(int curr_state_ind,
                       std::vector<int> &successors,
                       std::vector<double> &costs) override {
        if (manipulation_type_->getSpaceType() == BulletManipulationType::SpaceType::ConfigurationSpace)
        {
            return getSuccessorsCs(curr_state_ind, successors, costs);
        }
        else
        {
            return getSuccessorsWs(curr_state_ind, successors, costs);
        }
    }

    bool calculateIK(StateType &ws_state, StateType &state) {
        double position[3];
        double orientation[4];
        for (int i{0}; i < 3; i++) {
            position[i] = ws_state[i];
        }
        btQuaternion q;
        q.setEulerZYX(static_cast<btScalar>(ws_state[5]),
                      static_cast<btScalar>(ws_state[4]),
                      static_cast<btScalar>(ws_state[3]));
        orientation[0] = q.w();
        orientation[1] = q.x();
        orientation[2] = q.y();
        orientation[3] = q.z();
//        scene_interface_->plotPose(robot_.ee);
        return scene_interface_->calculateIK(robot_, planning_group_, position, orientation, state);
    }

    bool Move(PathType &path) {
        return scene_interface_->Move(robot_, planning_group_, path);
    }

    bool Control(StateType &state, std::vector<int> &joint_idx) {
        PathType path;
        path.push_back(state);
        return scene_interface_->Move(robot_, planning_group_, path, joint_idx);
    }

    bool setToState(StateType &state) {
        return scene_interface_->setJointState(robot_->id, state);
    }

    bool setToState(StateType &state, std::vector<int> &joint_idx) {
        return scene_interface_->setJointState(robot_->id, state, joint_idx);
    }

    bool getCurrentJointState(StateType &state) {
        state.clear();
        return scene_interface_->getCurrentJointState(robot_, planning_group_, state);
    }

    void ShortcutPath(PathType &path) {
        scene_interface_->ShortcutPath(robot_, planning_group_, path);
    }

    void densifyPath(const PathType &path, PathType& densified_path) {
        scene_interface_->densifyPath(path, densified_path);
    }

};
}